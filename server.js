const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const path = require('path');

const PORT = 3000;

const router = express.Router();

const DATA_FILE = path.join(__dirname, './data/healthcheck.json');

const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const webpackConfig = require('./webpack.config.js');

webpackConfig.entry.unshift(
  "webpack/hot/dev-server",
  "webpack-hot-middleware/client" 
);

const compiler = webpack(webpackConfig);

const app = express();

var _ = require('lodash');

// Bind application-level middleware to an instance
app.use(express.static(__dirname + '/www'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(function(req, res, next){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Origin", "Origin, X-Requested-With, token, Content-Type, Accept");
  next();
})

// ROUTES FOR OUR API
// =============================================================================

//FETCHING RESTFUL DATA
app.get('/api/status/', (req, res) => {
  fs.readFile(DATA_FILE, (err, data) => {
    res.json(JSON.parse(data));
  });
});

app.post('/api/login', function(req, res) {
  var user = {
    username: "leo",
    password: "click"
  };
  if(_.isEqual(user, req.body)) {
    return res.status(200).json({ hasAccess: true });
  } else {
    return res.status(404).json({ hasAccess: false });
  }
});

app.use(webpackDevMiddleware(compiler, {
  noInfo: true,
  hot: true,
  historyApiFallback: true,
  publicPath: webpackConfig.output.publicPath,
  filename: 'bundle.js',
  contentBase: 'www/',
  stats: {
    colors: true,
  },
  watchOptions: {
    aggregateTimeout: 300,
    poll: true
  },
}));

app.use(webpackHotMiddleware(compiler));

// Test route to make sure everything is working (accessed at GET http://localhost:8080/api)
const server = app.listen(PORT, function() {
  const host = server.address().address;
  const port = server.address().port;
  console.log('Example app listening at http://%s:%s', host, port);
});
