
## Welcome to Health Checker
---

### Setup
```
npm install
```
---

#### Start the development server with this command:
```
npm start
```
---

#### Test the development with this command:
```
karma start or karma start --auto-watch
```
---

### Compiling
#### Compile for production use only
```
npm run compile
```