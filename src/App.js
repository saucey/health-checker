/**
 * Created by leoscott on 25/05/2017.
 */
import Empty from './layout/EmptyLayout'
import Main from './layout/MainLayout'
import Login from './login/LoginContainer'
import Dashboard from './dashboard/DashboardContainer'
import React, { Component }  from 'react'
import { Match, Miss, Switch, Route, Redirect} from 'react-router'

class App extends Component {

  constructor(props, context) {
    super(props, context);
    console.log("context", this.context);
  }

  // componentWillReceiveProps(nextProps) {
  //   console.log(nextProps, 'life componentWillReceiveProps');
  // }

  render() {
    if (this.props.login) {
      return(
          <Main>
            <Route exact path='/' render={()=>(<Redirect to="/dashboard" />)} />
            <Switch>
              <Route path='/dashboard' component={Dashboard}/>
            </Switch>
          </Main>
      )
    } else {
      return (
          <Empty>
            <Route path="/same" render={() => <Redirect to="/same"/>} />
            <Route path="/*" render={() => <Redirect to="/"/>} />
            <Switch>
              <Route exact path="/same" render={() => alert('this is the same')} />
              <Route exact path="/*" component={Login} />
            </Switch>
          </Empty>
      )
    }
  }
}

export default App

