// import React from 'react';
import React, { Component } from 'react';
import { Table, pageHeader } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import client from '../client'

const statusColor = (status) => {
  switch ('' + status) {
    case 'Connected':
      return 'info';

    case 'Up':
      return 'success';

    case 'Disconnected':
    case 'Unknown':
      return 'warning';

    case '403':
    case '404':
      return 'danger';

    case 'Not Reqd':
      return 'active';

    default:
      return '';
  }
}

class dashboard extends Component {
  constructor(props) {
    super(props)
    this.state = {
      dataMatrix:[]
    }
    this.loadStatusDataFromServer = this.loadStatusDataFromServer.bind(this);
  }

  componentDidMount() {
    this.loadStatusDataFromServer();
    setInterval(this.loadStatusDataFromServer, 5000);
  }

  loadStatusDataFromServer() {
    client('api/status', 'get')
      .then(payload => {
        this.setState({
          dataMatrix: payload,
          error: null
        })
      })
      .catch(error => {
        this.setState({
          error
        })
      })
  }

  render() {

    const errorMsg = <div>Sorry! You are not connected.</div>

    /*const statuses = (
      <Table striped bordered condensed hover>
        <thead>
          <tr>
            <th className="text-center" width="25%">URL</th>
            <th className="text-center">PORT</th>
            <th className="text-center" colSpan="5">HOST</th>
          </tr>
        </thead>
        <tbody>
          {this.state.dataMatrix.map( row => (
            <tr key={row.id}>
              <td>{row.url}</td>
              <td>{row.port}</td>
              <td className={statusColor(row.status1)}>{row.status1}</td>
              <td className={statusColor(row.status2)}>{row.status2}</td>
              <td className={statusColor(row.status3)}>{row.status3}</td>
              <td className={statusColor(row.status4)}>{row.status4}</td>
              <td className={statusColor(row.status5)}>{row.status5}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    );*/

    const statuses = (
      <BootstrapTable 
        data={ this.state.dataMatrix }
        keyField="id"
        striped
        hover
        condensed
        pagination
        insertRow
        deleteRow
        search
      >
        <TableHeaderColumn dataField='url' dataAlign="center" width='25%' dataSort>URL</TableHeaderColumn>
        <TableHeaderColumn dataField='port' dataAlign="center" width='8%' dataSort>PORT</TableHeaderColumn>
        <TableHeaderColumn dataField='host'dataAlign="center" colSpan='5' dataSort>HOST</TableHeaderColumn>
      </BootstrapTable>
    );

    return (
      <div className="dashboard-ui">
        <div className="pageHeader">
          <h1>Dashboard</h1>
        </div>
        <div className="table-responsive">
          {this.state.error ?  errorMsg : statuses}
        </div>
      </div>
    );
  }

} // End Component  

export default dashboard;
