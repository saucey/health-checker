export const CONNECTED = 'CONNECTED';
export const UP = 'UP';
export const DISCONNECT_UNKNOWN = 'DISCONNECT_UNKNOWN';
export const FORBIDDEN_NOTFOUND = 'FORBIDDEN_NOTFOUND';
export const NOT_REQUIRED = 'NOT_REQUIRED';

export const statusConnected = () => ({
  type: 'CONNECTED',  
});

export const statusUp = () => ({
  type: 'UP',  
});

export const statusWarning = () => ({
  type: 'DISCONNECT_UNKNOWN',  
});

export const statusDanger = () => ({
  type: 'FORBIDDEN_NOTFOUND',  
});

export const statusRequired = () => ({
  type: 'NOT_REQUIRED',  
});
