import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { statusConnected, statusUp, statusWarning, statusDanger, statusRequired } from './DashboardActions'
import Dashboard from './Dashboard'

const mapDispatchToProps = dispatch => bindActionCreators({
  statusConnected,
  statusUp,
  statusWarning,
  statusDanger,
  statusRequired
}, dispatch)

const mapStateToProps = state => ({
  statusConnected,
  statusUp,
  statusWarning,
  statusDanger,
  statusRequired
}, state)

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard)