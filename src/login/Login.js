import React, { Component } from 'react'
import { Button, Modal, FormGroup, ControlLabel, FormControl, Form } from 'react-bootstrap';
import FieldGroup from '../partials/FieldGroup'

const isNumValid = num => (num !== '' && num > -1 && num < 11)
const isUsernameValid = val => (val !== '')
const isPassValid = val => (val !== '')

class login extends Component {

  constructor(props) {
    super(props)
    this.state = {
      isValid: false,
      username: null,
      password: null
    }
  }

  onChange() {

    // const num = this.inputNumber.value
    const password = this.inputPassword.value
    const username = this.inputUsername.value

    this.setState( state => ({
      ...state,
      isValid: isPassValid(password) && isUsernameValid(username),
      username: username,
      password: password
    }))
  }

  onSubmit (e) {
    e.preventDefault()
    this.props.onSubmit(this.state)
  }

  testPurpose() {
    return false
  }

  render () {
    return (
    <div>
      <Modal show={this.props.loginModal} onHide={this.props.closeModal} bsSize="small">
        <Modal.Header>
          <Modal.Title>Login</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={this.onSubmit.bind(this)}>
            <FieldGroup
                id="text"
                type="text"
                label="Username"
                placeholder="Username"
                inputRef={ elm => { this.inputUsername = elm } }
                onChange={this.onChange.bind(this)}
            />
            <FieldGroup
                id="password"
                type="password"
                label="Password"
                placeholder="*********"
                inputRef={ elm => { this.inputPassword = elm } }
                onChange={this.onChange.bind(this)}
            />
            <Button disabled={!this.state.isValid} className="login-btn btn btn-success" type="submit">
              LOGIN
            </Button>
          </Form>
        </Modal.Body>
      </Modal>
    </div>
    )
  }
}

export default login

