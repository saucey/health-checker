const initialState = true

const loginModal = (state = initialState, action) => {
  switch (action.type) {
    case 'OPEN':
      console.log(action, 'action');
      return true

    case 'CLOSE':
      console.log(action, 'action');
      return false

    default:
      return state

  }
}

export default loginModal
