export const openModal = () => ({
  type: 'OPEN',
})

export const closeModal = () => ({
  type: 'CLOSE',
})

export const submitUser = user => ({
  type: 'SUBMIT_LOGIN_USER',
  user
})