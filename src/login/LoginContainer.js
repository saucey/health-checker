import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { openModal, closeModal, submitUser } from './LoginActions'
import Login from './Login'
import App from '../App'

const mapDispatchToProps = dispatch => bindActionCreators({
  openModal,
  closeModal,
  onSubmit: submitUser
}, dispatch)

const mapStateToProps = state => ({
  loginModal: state.loginModal,
  login: state.login
}, state)



export default connect(mapStateToProps, mapDispatchToProps)(Login)