import React from 'react'
import 'whatwg-fetch'
import { Switch, Route } from 'react-router-dom'
import  { browserHistory, withRouter, Redirect }  from "react-router";

const mw = store => next => action => {
  
  if(action.type == 'SUBMIT_LOGIN_USER') {
    var payload = {
      username: action.user.username,
      password: action.user.password
    };

    return fetch('/api/login', {
      method: 'POST',
      mode: 'cors',
      redirect: 'follow',
      crossOrigin: true,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(payload)
    })
    .then(
      r => (r.json())
    )
    .then(function(response) {
      console.log('status: ', response.status)
      console.log(response.hasAccess, 'new response')
      action.user.isValid = response.hasAccess
      next(action)
    })
    .catch(function(err) {
      // Error :(
      console.info(err);
    });
  } else {
    next(action)
  }
}

export default mw
