import React from 'react';
import { render } from 'react-dom';
import Index from './index';
import store from './store';

render(
    <Index store={store}/>,
    document.getElementById('mount')
);
