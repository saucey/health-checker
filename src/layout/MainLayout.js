
import React, { Component } from 'react';
import Header from '../common/HeaderContainer'
import Sidebar from '../common/Sidebar'
import Footer from '../common/Footer'
import WrapperContainer from '../common/WrapperContainer'

class MainLayout extends Component {
  render() {
    let year = new Date().getFullYear();
    return (
      <WrapperContainer>
        <Header />
        <Sidebar />
          <div id="page-content-wrapper">
            <div className="container-fluid">
              <div className="row">
                <main className="container">
                  {this.props.children}
                </main>
              </div>
            </div>
          </div>
        <Footer year={year} />
      </WrapperContainer>
    );
  }
}
export default MainLayout;