import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { bindActionCreators } from 'redux'

import App from './App'

const mapStateToProps = state => ({
  login: state.login
}, state)

export default withRouter(connect(mapStateToProps)(App))
