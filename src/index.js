import  '../assets/styles.scss'
import React from 'react'
import App from './AppContainer'
import { Provider } from 'react-redux'
import { HashRouter, Route, Switch, Redirect } from 'react-router-dom'

const Index = ({store}) => (
    <Provider store={store}>
      <HashRouter>
        <App/>
      </HashRouter>
    </Provider>
);


export default Index

