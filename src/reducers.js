import { combineReducers } from 'redux'

import login from './login/loginReducer'
import loginModal from './login/loginModalReducer'
import dashboard from './dashboard/dashboardReducer'
import header from './common/headerReducer'

export default combineReducers({
  login,
  loginModal,
  dashboard,
  header
})
