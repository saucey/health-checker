import { createStore, applyMiddleware } from 'redux'
import reducers from './reducers'
import LoginMiddleware from './login/LoginMiddleware'

export default createStore(
    reducers,
    applyMiddleware(LoginMiddleware)
)