import {
  TOGGLE_MENU
} from './HeaderActions';

const initialState = false;
const header = (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_MENU:
      return state = !state;

    default:
      return state
  }
}

export default header
