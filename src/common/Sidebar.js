import React, { Component } from 'react';
import Header from './Header';
import { Nav, NavItem, NavDropdown, MenuItem} from 'react-bootstrap';

const Sidebar = () =>
  <div id="sidebar-wrapper">
    <Nav className="sidebar-nav">
      <NavItem eventKey={1} href="#" className="sidebar-brand">Wipro Dash</NavItem>
      <NavItem eventKey={2} href="#">Dashboard</NavItem>
      <NavItem eventKey={3} href="#">Overview</NavItem>
      <NavItem eventKey={4} href="#">Events</NavItem>
      <NavItem eventKey={5} href="#">Settings</NavItem>
      <NavItem eventKey={6} href="#">Profile</NavItem>
      <NavItem eventKey={7} href="#">Help</NavItem>
      <NavDropdown eventKey={8} title="Dropdown" id="basic-nav-dropdown">
        <MenuItem eventKey={8.1}>Action</MenuItem>
        <MenuItem eventKey={8.2}>Another action</MenuItem>
        <MenuItem eventKey={8.3}>Something else here</MenuItem>
        <NavDropdown eventKey={9} title="Dropdown" id="basic-nav-dropdown">
          <MenuItem eventKey={9.1}>Action</MenuItem>
          <MenuItem eventKey={9.2}>Another action</MenuItem>
          <MenuItem eventKey={9.3}>Something else here</MenuItem>
          <MenuItem divider />
          <MenuItem eventKey={9.4}>Separated link</MenuItem>
          <MenuItem eventKey={9.5}>Separated link</MenuItem>
        </NavDropdown>
      </NavDropdown>
    </Nav>
  </div>

export default Sidebar;