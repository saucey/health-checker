export const TOGGLE_MENU = 'TOGGLE_MENU';

export const onButtonToggle = () => ({
  type: 'TOGGLE_MENU'
})