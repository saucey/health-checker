import React from 'react'

const year = new Date().getFullYear();

const Footer = () =>
  <footer className="footer">
    <div className="container">
      <div className="row footerwrap">
        <div className="col-xs-12 col-sm-6 col-lg-9">
          <div id="footer" className="pull-left app-label">
            <h6 className="text-center">Dashboard Connectivity Status</h6>
          </div>
         </div>
         <div className="col-xs-12 col-sm-6 col-lg-3">
          <div className="pull-right copyright">
            <h6 className="text-center"><strong>Copyright</strong> Wipro for Lloyds TSB &copy;{year}</h6>
          </div>
        </div>
      </div>
    </div>
  </footer>

export default Footer;
