import React, { Component } from 'react';
import { Navbar, Nav, FormGroup, Form, FormControl, Button} from 'react-bootstrap';

class Header extends Component {
  constructor(props) {
    super(props);
  }

  onSubmit(e) {
    e.preventDefault();
    this.props.onButtonToggle();
  }

  render() {
    return (
      <Navbar inverse fixedTop fluid>
        <Navbar.Header>
          <Button
            id="menu-toggle"
            type="submit"
            className="navbar-toggle collapsed"
            onClick={this.onSubmit.bind(this)}>
            <span className="sr-only">Toggle navigation</span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
          </Button>
        </Navbar.Header>
        <Navbar.Form pullRight>
          <FormGroup>
            <FormControl type="text" placeholder="Search..." />
            <Button type="submit" className="search">
              <span className="magnifier"><i className="fa fa-search" aria-hidden="true"></i></span>
            </Button>
          </FormGroup>
          {' '}          
        </Navbar.Form>
      </Navbar>
    );
  }
}

export default Header;