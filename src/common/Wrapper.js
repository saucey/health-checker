import React, {Component} from 'react';

class Wrapper extends Component {
  render() {
    return (
      <div id="wrapper" className={this.props.header ? 'toggled' : null}>
        {this.props.children}
        </div>
    );
  }
}

export default Wrapper;