import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { onButtonToggle } from './HeaderActions'
import Header from './Header'

const mapDispatchToProps = dispatch => bindActionCreators({
  onButtonToggle,
}, dispatch)

const mapStateToProps = state => ({
  header: state.header,
}, state)

export default connect(mapStateToProps, mapDispatchToProps)(Header);