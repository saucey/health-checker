import React from 'react';
import { connect } from 'react-redux';
import Wrapper from './Wrapper';

const mapStateToProps = state => ({
  header: state.header
}, state)

const WrapperContainer = connect(mapStateToProps)(Wrapper);

export default WrapperContainer;