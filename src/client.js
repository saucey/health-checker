 const checkStatus = (response) => {
  if (response.status >= 200 && response.status < 300) {
    return response;
  } else {
    const error = new Error(`HTTP Error ${response.statusText}`);
    error.status = response.statusText;
    error.response = response;
    console.log(error);
    throw error;
  }
}

const parseJSON = (response) => {
  return response.json();
}

export default (url, method) => {
  return fetch(url, {
    method,
    headers: {
      'Accept': 'application/json'
    }
  })
  .then(checkStatus)
  .then(response => response.json());
};