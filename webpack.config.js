process.traceDeprecation = true// removes Deprecation error
const path = require('path');
const webpack = require('webpack');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');

const HOST = 'localhost';
const PORT = 3000;
const PROXY = `http://${HOST}:${PORT}/`;
const SERVER = { baseDir: ['www'], directory: true };

const plugins = [
  new webpack.optimize.OccurrenceOrderPlugin(),
  new webpack.DefinePlugin({
    'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development')
  }),
  new webpack.HotModuleReplacementPlugin(),
  new BrowserSyncPlugin({
    host: HOST,
    port: PORT,
    proxy: {
      target: PROXY,
      ws: true
    },
    reload: false,
    files: [
      'src/*.js',
      'assets/*.scss',
      'www/*.js',
      'www/*.html'
    ],
    // for docker (on virtulabox vm) mounted volume
    "watchOptions": {
      "usePolling": true,
      "interval": 500
    },
  }),
];

module.exports = {
  devtool: 'inline-source-map',
  context: path.join(__dirname, 'src'),
  entry: [
    './Main.js',
  ],
  output: {
    path: path.join(__dirname, 'www'),
    filename: 'bundle.js',
  },
  watch: true,
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          {
            loader: "style-loader" // creates style nodes from JS strings
          },
          {
            loader: "css-loader" // translates CSS into CommonJS
          },
          {
            loader: "sass-loader" // compiles Sass to CSS
          },
        ]       
      },
      {
        test: /\.(woff|woff2|ttf|svg|eot)$/,
        loader: 'url-loader?limit=100000',
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          'babel-loader',
        ],
      },
      {
        enforce: 'post',
        test: /\.(js)$/,
        use: [{
          loader: 'istanbul-instrumenter-loader'          
        }],                
        exclude: [/node_modules/, /\.spec\.(js)$/, /test/]
      },
    ],
  },
  plugins: plugins,
  externals: {
    'jsdom': 'window',
    'cheerio': 'window',
    'react/addons': true,
    'react/lib/ExecutionEnvironment': true,
    'react/lib/ReactContext': true
  },
  resolve: {
    modules: [
      path.join(__dirname, 'node_modules'),
    ],
  },
  target: 'web'
};
