# Dockerfile
FROM node:7-alpine

# Create app directory
RUN mkdir -p /app


#docker run --rm <image> <command>
# can remoce WORKDIR and use ./ as working dir in the COPY

# Build image :
# docker build -t <imagename> .

# interactive terminal + bash :
## docker run -it <imagename> bin/sh
WORKDIR /app

ENV PATH=$PATH:/app/node_modules/.bin

# Install app dependencies
COPY package.json /app/
RUN npm install && npm rebuild node-sass --force

# Bundle app source
COPY . /app

EXPOSE 3000

ENTRYPOINT ["npm", "run"]

# defined in package.json
CMD ["start"]