var webpackConfig = require('./webpack.config.js');

module.exports = function(config) {
  config.set({
    basePath: '',
    framework: ['jasmine'],
    files: [
      './src/**/*spec.js',
      'tests.webpack.js'
    ],
    plugins: [
      'karma-webpack',
      'karma-jasmine-html-reporter',
      'karma-coverage',
      'karma-jasmine',
      'karma-phantomjs-launcher',
      'karma-chrome-launcher',
      'karma-sourcemap-loader'
    ],
    preprocessors: {
      './src/**/*spec.js': [ 'webpack', 'sourcemap' ]
    },
    webpack: webpackConfig,
    webpackServer: {
      noInfo: true //Prevent spam of console when running in karma!
    },
    reporters: [ 'progress', 'coverage' ],
    postLoaders: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'istanbul-instrumenter-loader'
    }],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['Chrome'],
    singleRun: false,
    captureTimeout: 60000,
    coverageReporter: {
      type: 'html', //produces a html document after code is run
      dir: 'coverage/' //path to created html doc
    },
    coverageIstanbulReporter: {
      reports: [ 'text-summary' ],
      fixWebpackSourcePaths: true
    }
  });
};